## Compositon Exercise 1
It appears the following 11 yml files for Exercise 1:

basic-auth-<number>.yml

Also, there is another file where commands to be executed:

commands_to_be_executed.txt

Inside commands_to_be_executed.txt it appears the commands to executed in terraform console.

You have to executed this kind of commands: 

terraform import kubernetes_secret.74623 default/my-secret_74623

These commands will create the following secret called default/my-secret_xxxx in order to keep safeguarding their locations that it was the goal of this exercise 1.

